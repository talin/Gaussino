# Simulation
Configuration related to the simulation phase.


```{eval-rst}
.. currentmodule:: Gaussino.Simulation

.. autoclass:: SimPhase
   :show-inheritance:
   :members: setOtherProp, setOtherProps, configure_phase
   :undoc-members:
   :special-members: __apply_configuration__
```
