.. Gaussino documentation master file, created by
   sphinx-quickstart on Fri Apr 30 15:52:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Gaussino's documentation!
====================================
.. toctree::
   :caption: About
   :hidden:
   :maxdepth: 2

   about/gaussino

.. toctree::
   :caption: Getting started
   :maxdepth: 2

   getting_started/gaussino.md
   getting_started/gauss.md
   getting_started/contributing.md

.. toctree::
   :caption: Configuration
   :maxdepth: 2

   configuration/gaussino
   configuration/generation
   configuration/simulation
   configuration/external_detector
   configuration/parallel_geometry
   configuration/gdml

.. toctree::
   :caption: Examples
   :maxdepth: 2
   
   examples/external_detector
   examples/parallel_geometry
   examples/adding_subdetector_in_detector
   examples/adding_subdetector_in_dd4hep


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
