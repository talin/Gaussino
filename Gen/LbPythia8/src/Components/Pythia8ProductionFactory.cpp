/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LbPythia8.
#include "LbPythia8/Pythia8Production.h"
#include "LbPythia8/Pythia8ProductionMT.h"

// Declare the Pythia8Production tool.
DECLARE_COMPONENT( Pythia8Production )

// Declare the Pythia8ProductionMT tool.
DECLARE_COMPONENT( Pythia8ProductionMT )
